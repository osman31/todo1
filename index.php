<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="style.css">
    <title>TODO</title>
</head>
<?php
require "connection.php";
$alltasks = "SELECT  *FROM tasks";
$resalltasks = $mysqli->query($alltasks);
if (isset($_POST['delete'])) {
    $id = (int)$_POST['test'];
    $deltask = "DELETE FROM tasks WHERE id=$id";
    $resdeltask = $mysqli->query($deltask);
    header("location: index.php");
}
?>

<body>
    <div class="tu_link">
        <h2> <a href="/index.php">Tasks </a></h2>
        <h2> <a href="/user.php">USERS</a> </h2>
    </div>
    <div>
        <table>
            <thead>
                <tr>
                    <th colspan="4">TODO</th>
                </tr>
                <tr style="border: 2px solid;">
                    <th>ID</th>
                    <th>Tasks</th>
                    <th colspan="2">Edit / Delete</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($resalltasks as $res) { ?>
                    <tr>
                        <td><?php echo $res['id']; ?></td>
                        <td><?php echo $res['task']; ?></td>

                        <form method="POST" action="modify.php">
                            <td> <button class="btn_eit" type="submit" name="id" value="<?php echo (int)$res['id']; ?>">Edit</button></td>
                        </form>
                        <form method="POST">
                            <input type="hidden" value="<?php echo (int)$res['id'] ?>" name="test">
                            <td> <button class="btn_del" type="submit" name="delete">Delete</button></td>
                        </form>
                    </tr>
                <?php }; ?>
            </tbody>
        </table>
    </div>
    <div class="btn_opn">
        <div>
            <form method="POST" action="./modify.php">
                <button class="btn_add" type="submit" name="add">Add New Task</button>
            </form>
        </div>
        <div>
            <form method="POST" action="./user.php">
                <button class="btn_add" type="submit" name="assign_task">Assign Task</button>
            </form>
        </div>
    </div>
</body>

</html>