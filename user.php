<?php
require "connection.php";
$resalluser = $mysqli->query("SELECT *FROM users");
$resalltasks = $mysqli->query("SELECT  *FROM tasks");
if (isset($_POST['add'])) { ?>
    <form method="POST">
        <input type="text" name="first_name" placeholder="First name:"><br>
        <input type="text" name="last_name" placeholder="Last name:"><br>
        <input class="btn_add" type="submit" value="Add New User" name="add_user">
    </form>
<?php
}
if (!empty($_POST['add_user'])) {
    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];
    $mysqli->query("INSERT INTO users (fname,lname) VALUES ('$fname', '$lname') ");
    header('location: user.php');
}

if (isset($_POST['id'])) {
    $id = (int)$_POST['id'];
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
?>
    <form method="POST">
        <p>first name:</p>
        <input type="text" name="fname" value="<?php echo $fname; ?>"><br>
        <p>last name:</p>
        <input type="text" name="lname" value="<?php echo $lname; ?>"><br>
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <input class="btn_eit" type="submit" value="Edit it" name="edit_user">
    </form>
<?php
}
if (isset($_POST['edit_user'])) {
    echo $id = (int)$_POST['id'];
    echo  $fname = $_POST['fname'];
    echo $lname = $_POST['lname'];
    $users = $mysqli->query("UPDATE users SET fname = '$fname', lname= '$lname' WHERE id =" . (int)$id);
    header('location: user.php');
}
if (isset($_POST['delete'])) {
    $id = (int)$_POST['id'];
    $deleteuser = $mysqli->query("DELETE FROM users WHERE id = $id");
    header('location: user.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>USERS</title>
</head>

<body>
    <div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th colspan="5">USERS LIST</th>
                    </tr>
                    <tr style="border: 2px solid;">
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th colspan="2">Edit / Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($resalluser as $res) { ?>
                        <tr>
                            <td><?php echo $res['id']; ?></td>
                            <td><?php echo $res['fname']; ?></td>
                            <td><?php echo $res['lname']; ?></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="fname" value="<?php echo $res['fname']; ?>" Required>
                                    <input type="hidden" name="lname" value="<?php echo $res['lname']; ?>" Required>
                                    <input type="hidden" name="id" value="<?php echo $res['id']; ?>">
                                    <button class="btn_eit" type="submit" name="edit">Edit</button>
                                </form>
                            </td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="id" value="<?php echo $res['id']; ?>" Required>
                                    <button class="btn_del" type="submit" name="delete">Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="btn_opn">
            <div>
                <form method="POST">
                    <button class="btn_add" type="submit" name="add">Add New User</button>
                </form>
            </div>
            <div>
                <form method="POST">
                    <button class="btn_add" type="submit" name="assign_task">Assign Task</button>
                </form>
            </div>
            <div>
                <form method="POST">
                    <button class="btn_eit" type="submit" name="modify_ass">Update Assign Task</button>
                </form>
            </div>
        </div>
    </div>
    <div class="assign">
        <?php
        if (isset($_POST['assign_task'])) { ?>
            <form method="POST">
                <div class="ch_box">
                    <p>SELECT YOU TASKS</p> <br />
                    <?php foreach ($resalltasks as $res) { ?>
                        <input type="checkbox" name="task[]" value="<?php echo $res['id']; ?>" /><?php echo $res['task']; ?><br />
                    <?php } ?>
                </div>
                <div class="usrlit">
                    <label for="user-select">Choose users:</label>
                    <select name="users" id="user-select">
                        <option value="">--Please select a User--</option>
                        <?php
                        foreach ($resalluser as $res) { ?>
                            <option name="user" value="<?php echo $res['id']; ?>" Required><?php echo $res['fname'] . "  " . $res['lname']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div>
                    <button type="submit" name="assign_it">Assign It</button>
                </div>
            </form>
        <?php }
        if (isset($_POST['assign_it'])) {
            $uid = $_POST['users'];
            $tid = $_POST['task'];
            foreach ($tid as $element) {
                $taskid = $element;
                $mysqli->query("INSERT INTO ustsk (userid,taskid) VALUES ('$uid', '$taskid')");
            }
        } ?>
    </div>

</body>

</html>